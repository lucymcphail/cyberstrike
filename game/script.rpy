﻿label start:

    play music "theme.ogg"

    python:
        povname = renpy.input("What's your name?")
        povname = povname.strip()

        if povname == "":
            povname = "Player"

        points = 0

    $ p = Character("[povname]")

    scene bg crash start

    scene bg crash 1
    with dissolve

    scene bg crash 2
    with dissolve

    scene bg crash 3
    with dissolve

    scene bg crash
    with dissolve

    scene bg crash hands
    with fade

    "Where am I?"

    scene bg crash faded
    with fade

    show lily neutral
    with dissolve

    define l = Character("Lily")

    l  "Hello there! My name’s Lily, what’s yours?"

    p "Hi Lily, I'm [povname], from planet Zorg. I appear to have crash landed here in this farm."

    "I wonder if this girl could help me get home..."

    p "If I can contact the other aliens who crashed in the asteroid storm, maybe we can get back to Zorg together."

    p "Do you know how I might be able to contact my friends?"

    show lily smile
    with dissolve

    l "I think so. Let me show you..."

    jump computer

label computer:

    scene bg computer blank
    with fade

    l "This is my computer. I think it might be able to help you find your way home."

    scene bg computer startup
    with dissolve

    l "I just need to turn it on..."

    show lily sad
    with dissolve

    l "..."

    hide lily
    with dissolve

    p "Alright, how do I use it?"

    l "Let's try looking up Zorg online. Maybe we can find out where it is."

    menu:

        "Choose a search engine"

        "Googli":

            jump googli

        "Bong":

            jump bong

label bong:

    l "Bong? Really? Are you suuuuure?"

    show lily angry
    with dissolve

    l "Bong never works!"

    p "Okay, I guess I'll try Googli then."

    "I can't help but wonder why she gave me the choice in the first place..."

    jump googli

label googli:

    scene bg computer googli

    "I wonder if I'll find anything..."

    scene bg computer googli search

    "..."

    scene bg computer googli error

    "Nothing."

    "I guess your people haven't found out about Zorg yet."

    l "Maybe we should try looking for Zorg on a map?"

    jump googlimaps


label googlimaps:

    scene bg computer googlimaps

    l "Try searching for Zorg to see if you can find it."

    python:

        searchchoice = renpy.input("Search Googli Maps")
        searchchoice = searchchoice.strip()

        while searchchoice.lower() != "zorg":
            searchchoice = renpy.input("Search Googli Maps (it might be a better idea to look for Zorg)")
            searchchoice = searchchoice.strip()

    scene bg computer googlimaps error

    "Sorry, but that's not available on Googli Maps."

    l "Oh yeah, I forgot to tell you, you can only search locations on Earth. Maybe you could try searching for your friend on Spacebook and they could help you get home."

    l "Maybe we should find some of your friends on Spacebook?"

    p "What's Spacebook?"

    l "It's a social network. It lets you talk to anyone in the world."

    p "Sounds Great! Let's try it."

    jump spacebook_signup


label spacebook_signup:

    scene bg computer spacebook signup

    "Lily opens Spacebook and navigates to the sign-up page."

    l "Before you use Spacebook, we need to create an account for you."

    p "Alright."

    menu:

        "Create your profile"

        "Create a detailed profile":

            $ points += 20
            jump spacebook

        "Create a private profile with as little information as possible":

            $ points += 5
            jump spacebook


label spacebook:

    scene bg computer spacebook main

    "Within 5 minutes I have already been flooded with friend requests."

    show lily neutral
    with dissolve

    l "Wow, you're popular!"

    show lily sad
    with dissolve

    l "More popular than me..."

    hide lily
    with dissolve

    menu:

        "Respond to friend requests"

        "Accept them all":

            $ points += 20
            jump accept_all

        "Look through them yourself":

            $ points += 5
            jump accept_some

label accept_all:

    "After sifting through all the spam I received, I find a message from my friend Kwarg."

    jump kwarg


label accept_some:

    "Amidst all the friend requests, I find one from my friend Kwarg."

    jump kwarg


label kwarg:

    scene bg space
    with fade

    "I send a message to Kwarg, asking him where he is."

    "Lily manages to convince her parents to take me to him."

    "Together, we build a spaceship capable of taking us back to Zorg."

    "After saying my final farewell to Lily, we take off and return home."

    "But now I know about the internet and Spacebook, I can contact her and Kwarg wherever I am."

    show lily neutral
    with dissolve

    l "Oh yeah, one last thing - can you get rid of these notifications for me?"

    hide lily
    with dissolve

    jump play_popup_game

label after_game:

    scene black
    with fade

    "The end"

    l "Wait a second, I'm not done!"

    show lily neutral
    with dissolve

    l "The internet is a fantastic place. It allows us to do wonderful things, like talking to people across the world."

    l "However, the internet is not without its downsides..."

    l "You may have noticed that you've been playing a game for the last five minutes."

    l "Even though it appeared to be about an alien crashing from space and learning to use the internet, really it has a deeper meaning."

    scene bg computer googli
    show lily neutral at right
    with fade

    l "The first thing you did on the computer was to try to find Zorg using a search engine."

    l "But did you know that with most popular search engines, the companies that run them store your search history?"

    l "If you use Google like most people, you can visit history.google.com to see all of your searches."

    l "It also shows you what your interests are, based on your searches and other web activity."

    l "The best thing to do is to use a search engine that respects your privacy, such as Duckduckgo."

    scene bg computer googlimaps
    show lily neutral at right
    with fade

    l "The next thing you did was look at an online map."

    l "..."

    show lily smile at right
    with dissolve

    l "Okay, that one was just part of the story. But it was fun, right?"

    scene bg computer spacebook main
    show lily neutral at right
    with fade

    l "The last thing you did was to find your friend Kwarg using a social media platform."

    l "These websites are notorious for storing your data."

    l "That said, you can't really blame them - some people document their entire lives there. It would be a waste of a perfectly good business oppurtunity."

    l "Obviously, there are other issues with social media, including identity theft."

    show lily smile at right
    with dissolve

    l "But those are issues for another game."

    show lily neutral at right
    with dissolve

    l "Really, there isn't much you can do about this one short of staying away from social media entirely."

    l "Not that I'm expecting you to do that, [povname]. I don't blame you for not wanting to quit social media."

    l "But if you care about your privacy, you'll have to wait for more respectful social networks or tighter legislation."

    scene bg crash faded
    show lily smile
    with fade

    l "Anyway, I hope you enjoyed this little game. Thanks for playing!"

    return
